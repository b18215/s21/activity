let users = ["Dwayne Johnson","Steve Austin","Kurt Angle","Dave Bautista"];

console.log("Original Array:")
console.log(users);

/*
    1. Create a function which is able to receive a single argument and add the input at the end of the users array.
        -function should be able to receive a single argument.
        -add the input data at the end of the array.
        -The function should not be able to return data.
        -invoke and add an argument to be passed in the function.
        -log the users array in the console.

*/

function addData(datadd){
    users[users.length] = datadd;
    console.log("latest user register");
    console.log(users);

};
   addData("Hulk Hoggan");



/*
    2. Create function which is able to receive an index number as a single argument return the item accessed by its index.
        -function should be able to receive a single argument.
        -return the item accessed by the index.
        -Create a global variable called outside of the function called itemFound and store the value returned by the function in it.
        -log the itemFound variable in the console.

*/

function viewIndexEntry(indexNum){
    let i = indexNum;
    let indexData = users[i];
    console.log("The data index selected is: " + indexData);


};
   //(Input Index Number)
   viewIndexEntry(0);


/*
    3. Create function which is able to delete the last item in the array and return the deleted item.
        -Create a function scoped variable to store the last item in the users array.
        -Shorten the length of the array by at least 1 to delete the last item.
        -return the last item in the array which was stored in the variable.

*/

function removeLastEntry(){
    console.log("user register before removal of last entry");
    console.log(users);
    users.length = users.length-1
    console.log("latest user register after removal of last entry");
    console.log(users);

};
   removeLastEntry();


/*
    4. Create function which is able to update a specific item in the array by its index.
        -Function should be able to receive 2 arguments, the update and the index number.
        -First, access and locate the item by its index then re-assign the item with the update.
        -This function should not have a return.
        -Invoke the function and add the update and index number as arguments.
        -log the users array in the console.

*/

function assignNewEntries(indexNumber, dataEntry){
    let i = indexNumber;


    console.log("user register before assignment of new entry");
    console.log(users);
    users[i] = dataEntry
    console.log("user register after assignment of new entry");
    console.log(users);

};
   //(Index Number, "Wrestle")
   assignNewEntries(0, "Tripple-H");
   



/*
    5. Create function which is able to delete all items in the array.
        -You can modify/set the length of the array.
        -The function should not return anything.

*/

function deleteAll (){
    console.log("user register before deleting all data");
    console.log(users);
    users = ""
    console.log("user register after deleting all data");
    console.log(users);

};
   deleteAll();


/*
    6. Create a function which is able to check if the array is empty.
        -Add an if statement to check if the length of the users array is greater than 0.
            -If it is, return false.
        -Else, return true.
        -Create a global variable called outside of the function  called isUsersEmpty and store the returned value from the function.
        -log the isUsersEmpty variable in the console.

*/

function checkUsersArray (){
    console.log("check user register entries");
    console.log(users);

    console.log("check user register length");
    console.log(users.length);

    if( users.length > 0){
            console.log(false)
    } else{
        console.log(true)
    }

};
   checkUsersArray();



/*

    Stretch Goal:

    Create a function which is able to get the index number of an item.
        -The function should be able to receive a single argument. The data input will be matched against each item in the array, if the input matched an item in the array we will return the current index number else we will return -1
        -Add a for loop to iterate over each item in the array.
        -in the for loop, add an if statement to check if the input item is the same as array item currently being iterated or looped.
        -if it matches, return the value of the index or the value of the variable used as a counter for the loop.
        -add an Else statement to return -1, if otherwise.
        -Create a global variable called outside of the function indexOfItem and store the returned value of the function.
        -log the value of the indexOfItem in the console.
*/